module types

  type array
     double precision, pointer :: x(:) => null()
  end type array
  
contains

  subroutine new_array(this, x)
    type(array), intent(inout) :: this
    double precision, intent(in), target, optional :: x(:)
    if (present(x)) this % x => x
  end subroutine new_array
  
end module types
