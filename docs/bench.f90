module test

contains

  subroutine kernel(n, x, y)
    integer, intent(in) :: n
    real(8), intent(in) :: x
    real(8), intent(out) :: y
    integer :: m
    m = n * 2
    y = log(exp(-x**m))
  end subroutine kernel

  subroutine main(n, z)
    integer, intent(in) :: n
    real(8), intent(out) :: z
    real(8) :: x, y
    integer :: i, k=4
    z = 0
    do i = 1, n
       do j = 1, n
          call kernel(k, x, y)
          z = z + y
       end do
    end do
  end subroutine main

end module test

