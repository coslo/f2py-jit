# Changelog

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.2.0 - 2025/02/02

[Full diff](https://framagit.org/coslo/f2py-jit/-/compare/1.1.1...1.2.0)

###  New
- Add `inline_include` and `inline_exclude` in `jit()` and `build_module()`
###  Bug fixes
- Fix compilation with long lines
- Fix missing info on compilation error when `verbose` is `False`

## 1.1.1 - 2024/12/14

[Full diff](https://framagit.org/coslo/f2py-jit/-/compare/1.1.0...1.1.1)

###  Bug fixes
- Fix issues with Python 3.11

## 1.1.0 - 2024/10/26

[Full diff](https://framagit.org/coslo/f2py-jit/-/compare/1.0.1...1.1.0)

###  New
- Support Python versions >=3.12
- Add `backend` argument to control the build backend 

## 1.0.1 - 2024/10/14

[Full diff](https://framagit.org/coslo/f2py-jit/-/compare/1.0.0...1.0.1)

###  Bug fixes
- Fix setuptools dependence issue

## 1.0.0 - 2024/10/12

[Full diff](https://framagit.org/coslo/f2py-jit/-/compare/0.11.0...1.0.0)

### Changes
- Derived types support is now optional via `f2py-jit[types]`
- Python compatibility is between 3.7 and 3.11 (from 3.8 for `f2py-jit[types]`)

###  Bug fixes
- Fix types dependencies: only works >3.7 and setuptools<74
- Fix numpy 2 compatibility
- Use `pyproject.toml`

## 0.11.0 - 2024/03/12

[Full diff](https://framagit.org/coslo/f2py-jit/-/compare/0.10.3...0.11.0)

###  New
- Add `inline` argument di `build_module()`

###  Bug fixes
- Fix `jit()` inline argument with multiple files
- Fix CI: highest supported python version is 3.11

## 0.10.3 - 2024/02/22

[Full diff](https://framagit.org/coslo/f2py-jit/-/compare/0.10.2...0.10.3)

###  Bug fixes
- Fix issues when changing compiler arguments
- Fix cleaning of temporary folders with inlining

## 0.10.2 - 2023/11/26

[Full diff](https://framagit.org/coslo/f2py-jit/-/compare/0.10.1...0.10.2)

###  Bug fixes
- Fix removal of temporary directories in /tmp

## 0.10.1 - 2023/08/09

[Full diff](https://framagit.org/coslo/f2py-jit/-/compare/0.10.0...0.10.1)

###  Bug fixes
- Fix and improve caching using proper locking

## 0.10.0 - 2023/07/27

[Full diff](https://framagit.org/coslo/f2py-jit/-/compare/0.9.2...0.10.0)

###  New
- Add skip and only arguments to jit()

## 0.9.2 - 2023/04/15

[Full diff](https://framagit.org/coslo/f2py-jit/-/compare/0.9.1...0.9.2)

###  Bug fixes
- Fix docs

