API reference
-------------

``f2py-jit`` module
~~~~~~~~~~~~~~~~~~~
.. automodule:: f2py_jit.f2py_jit
		:members:

``finline`` module
~~~~~~~~~~~~~~~~~~
.. automodule:: f2py_jit.finline
		:members:

