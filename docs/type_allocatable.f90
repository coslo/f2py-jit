module types

  type array
     double precision, allocatable :: x(:)
  end type array
  
contains

  subroutine new_array(this, n)
    type(array), intent(inout) :: this
    integer,     intent(in), optional :: n
    if (present(n)) allocate(this % x(n))
  end subroutine new_array
  
end module types
