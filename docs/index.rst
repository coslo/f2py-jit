========
f2py-jit
========

.. image:: https://img.shields.io/pypi/v/f2py-jit.svg
    :target: https://pypi.python.org/pypi/f2py-jit/
    :alt: pypi

.. image:: https://img.shields.io/pypi/pyversions/f2py-jit.svg
    :target: https://pypi.org/project/f2py-jit/
    :alt: Version

.. image:: https://img.shields.io/pypi/l/f2py-jit.svg
    :target: https://pypi.org/project/f2py-jit/
    :alt: License

.. image:: https://framagit.org/coslo/f2py-jit/badges/master/pipeline.svg
    :target: https://framagit.org/coslo/f2py-jit/-/commits/master
    :alt: Pipeline

.. image:: https://framagit.org/coslo/f2py-jit/badges/master/coverage.svg
    :target: https://framagit.org/coslo/f2py-jit/-/commits/master
    :alt: Coverage
	  
``f2py-jit`` builds efficient Fortran extensions for Python at run time. It extends the machinery of `f2py <https://numpy.org/doc/stable/f2py/>`_ to provide

- Compilation of Fortran source blocks as Python strings
- Caching of module builds across executions
- Support for Fortran derived types via `f90wrap <https://github.com/jameskermode/f90wrap>`_
- Inlining to improve performance (experimental)

------------

.. toctree::
   :caption: Documentation
   :maxdepth: 2
   
   tutorial
   api   

.. toctree::
   :caption: Links

   Source Code <https://framagit.org/coslo/f2py-jit>
   Tutorial on Binder <https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fcoslo%2Ff2py-jit/HEAD?labpath=docs%2Findex.ipynb>
   Org-mode notebook <https://framagit.org/coslo/f2py-jit/-/blob/master/docs/tutorial.org>
   Jupyter notebook <https://framagit.org/coslo/f2py-jit/-/blob/master/docs/tutorial.ipynb>
   

  
