PROJECT = f2py_jit

.PHONY: all dist test coverage install docs clean

all: install

test:
	python -m unittest discover -s tests

coverage: install
	coverage run --source $(PROJECT) -m unittest discover -s tests; coverage report

install:
	pip install .

docs:
	#	pdoc -o docs/api --force --html --skip-errors $(PROJECT)
	#emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=docs/index.org -f org-rst-export-to-rst --kill
	emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=docs/tutorial.org -f 	org-rst-export-to-rst --kill
	orgnb docs/tutorial.org
	make -C docs/ singlehtml

pep8:
	autopep8 -r -i $(PROJECT)
	autopep8 -r -i tests
	flake8 $(PROJECT)

clean:
	rm -f ${PROJECT}/*pyc ${PROJECT}/*/*pyc tests/*pyc
