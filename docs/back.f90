module back
  implicit none
contains
  subroutine mysum(x, y, z)
    double precision, intent(in) :: x, y
    double precision, intent(out) :: z
    z = x + y
  end subroutine mysum
end module back

