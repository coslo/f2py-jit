subroutine addone(i, j)
  implicit none
  integer, intent(in) :: i
  integer, intent(out) :: j
  j = i+1
end subroutine addone
