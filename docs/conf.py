# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../'))

project = 'f2py-jit'
html_title = 'f2py-jit documentation'
copyright = '2023, Daniele Coslovich'
author = 'Daniele Coslovich'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.autodoc', 'sphinx.ext.coverage', 'sphinx.ext.napoleon', 'sphinx.ext.viewcode']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The root document.
root_doc = 'index'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# This kills highlighting of source code via viewcode (which only works for html target)
# but is necessary to mute the colors of the results blocks
highlight_language = 'none'

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'furo'
html_static_path = ['_static/furo']
html_css_files = ['custom.css']
html_theme_options = {
    'source_repository': 'https://framagit.org/coslo/f2py_jit',
    "light_css_variables": {
            "admonition-title-font-size": "1rem",
            "admonition-font-size": "1rem",
        },
    }

# html_theme_options = {
#     'description': 'A just-in-time Fortran extension builder based on f2py',
#     'fixed_sidebar': True,
#     'sidebar_collapse': True,
#     'extra_nav_links': {'Run it on Binder': 'https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fcoslo%2Ff2py-jit/HEAD?labpath=docs%2Findex.ipynb',
#                         'Get it as org-mode or jupyter notebook': 'https://framagit.org/coslo/f2py-jit/-/tree/master/docs'},
#     'gray_2': '#F4F4F4ED',
#     'sidebar_width': '270px',
#     'body_max_width': 'auto',
#     'page_width': '1100px',
# #    'code_highlight_bg': '#111',
# }

# html_sidebars = {
#     '**': [
#         'about.html',
#         'navigation.html',
#         'searchbox.html',
#         'relations.html',
#         'donate.html',
#     ]
# }

